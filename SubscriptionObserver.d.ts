export declare interface SubscriptionObserver<T> {

    // Sends the next value in the sequence
    next(value: T): void;

    // Sends the sequence error
    // tslint:disable-next-line:no-any
    error(errorValue: any): void;

    // Sends the completion notification
    complete(): void;

    // A boolean value indicating whether the subscription is closed
    closed: boolean;
}
