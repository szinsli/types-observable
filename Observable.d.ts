import {SubscriberFunction} from './SubscriberFunction';
import {Subscription} from './Subscription';
import {Observer} from './Observer';

declare interface Observable<T> {

    // Subscribes to the sequence with an observer
    subscribe(observer : Observer<T>) : Subscription;

    // Subscribes to the sequence with callbacks
    subscribe(onNext : (value: T) => void,
              onError? : (error: any /* ideally, something of type Error */) => void,
              onComplete? : () => void) : Subscription;
    // [symbolObservable]() : Observable<T>;
}

declare namespace Observable {
    // Converts items to an Observable
    export function of<T>(...items: T[]): Observable<T>;
    // Converts an observable or iterable to an Observable
    export function from<T>(observable: Observable<T>): Observable<T>;
}

declare interface ObservableConstructor<T> {
    new<T> (subscriber: SubscriberFunction<T>): Observable<T>;
}
