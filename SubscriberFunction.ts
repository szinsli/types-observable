import {Subscription} from './Subscription';
import {SubscriptionObserver} from './SubscriptionObserver';

export declare interface SubscriberFunction<T> {
    (observer: SubscriptionObserver<T>): (() => void)| Subscription 
}