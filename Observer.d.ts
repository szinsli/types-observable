import {Subscription} from './Subscription';

declare interface Observer<T> {

    // Receives the subscription object when `subscribe` is called
    start?(subscription : Subscription): void;

    // Receives the next value in the sequence
    next?(value: T): void;

    // Receives the sequence error
    // tslint:disable-next-line:no-any
    error?(errorValue: any): void;

    // Receives a completion notification
    complete?(): void;
}
